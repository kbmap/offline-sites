FROM nginx:alpine

COPY conf.d /etc/nginx/conf.d
COPY public /public
COPY self-signed.conf /etc/nginx/self-signed.conf

EXPOSE 80 443
WORKDIR /public
