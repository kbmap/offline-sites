Offline Sites
=============

## Custom `/etc/hosts`

Add the following lines into `/etc/hosts`.
```
127.0.0.1	docs.python-requests.org
127.0.0.1	docs.python.org
127.0.0.1	getbootstrap.com
127.0.0.1	jekyllcn.com
127.0.0.1	jekyllrb.com
127.0.0.1	liquidmarkup.org
127.0.0.1	mypy-lang.org
127.0.0.1	semantic-ui.com
127.0.0.1	tornadoweb.org
127.0.0.1	yaml.readthedocs.io
```

## Install docker

```
$ curl -sSL https://get.docker.com/ | sh
$ docker --version
```

## Build docker image

```bash
$ curl -sSL https://gitlab.com/coy/offline-sites/raw/master/Dockerfile | docker build -t offline-sites -
$ docker images
```

> You can also use a pre-built docker image: [registry.gitlab.com/coy/offline-sites][4] ![][5].

## Run docker container

```bash
$ docker run --name offline-sites -v $(pwd)/certs:/etc/nginx/certs:ro -d -p 80:80 -p 443:443 offline-sites
$ docker ps
```

Where the certifacates for HTTPS are stores in `$(pwd)/certs`.

## Use docker-compose to manage (optional)

It is very handy to use [docker-compose][7] to manage docker containers.
You can download the binary at <https://github.com/docker/compose/releases>.

This is a sample `docker-compose.yml` file.

```yaml
offline-sites:
  image: registry.gitlab.com/coy/offline-sites
  ports:
    - "80:80"
    - "443:443"
  volumes:
    - ./certs:/etc/nginx/certs
  restart: always
```

It is highly recommended that you setup a directory tree to make things easy to manage.

```bash
$ mkdir -p ~/offline-sites/
$ cd ~/offline-sites/
$ curl -sSLO https://gitlab.com/coy/offline-sites/raw/master/docker-compose.yml
$ docker-compose up -d
$ docker-compose ps
```

[3]: https://github.com/docker/docker
[4]: https://gitlab.com/coy/offline-sites.git
[5]: https://gitlab.com/coy/offline-sites/badges/test/docker/pipeline.svg
[6]: https://duckduckgo.com/?q=password+12&t=ffsb&ia=answer
[7]: https://github.com/docker/compose
